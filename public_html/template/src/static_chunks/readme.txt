All chunks that cannot be called fully external should be made static.

1. Create a media source called "Chunks" with "/template/src/static_chunks/" as the basePath and baseUrl.
2. Set each chunk as "Is Static" select the "Chunks" media source, then enter the same chunk name with".html" at the end in the Static file field. e.g. "galleryThumbTpl.html".
3. Check that the static files are now being stored in this directory.

