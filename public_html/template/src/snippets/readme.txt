All snippets that cannot be called fully externally should be made static.

1. Create a media source called "Snippets" for with "/template/src/snippets/" as the basePath and baseUrl.
2. Set each snippet as "Is Static" select the "Snippets" media source, then enter the same snippet name with".html" at the end in the Static file field. e.g. "name.php".
3. Check that the static files are now being stored in this directory.

