All plugins that cannot be called fully externally should be made static.

1. Create a media source called "Plugins" for with "/template/src/plugins/" as the basePath and baseUrl.
2. Set each plugin as "Is Static" select the "Plugins" media source, then enter the same plugin name with".php" at the end in the Static file field. e.g. "name.php".
3. Check that the static files are now being stored in this directory.